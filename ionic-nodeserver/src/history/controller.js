
import transaction from "./transact";


module.exports = {
    
    getHistory: async (address,eventName,eventTopic) => {

           try {

            const eventLogs = await transaction.getAllEventLogs(eventTopic);
            const result =  await transaction.getEventsForAddress(address,eventLogs,eventName);
 
            return result;

        }
        catch (err) {
            throw err;
        }
    }
}