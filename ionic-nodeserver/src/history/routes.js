import express from "express";
import controller from "./controller";
const router = express.Router();
import config from '../config';

const events = config.events;

router.get('/', async (req, res, next) => {
    try {

        const data = req.query;
     
        let eventName = events[data.type].eventName;
        let eventTopic = events[data.type].eventTopic;
     
        const result = await controller.getHistory(data.address,eventName,eventTopic);

        res.status(200).send(result);
    
        
    }
    catch (e) {
        res.status(500).send(e);
    }
});





module.exports = router;