import express from "express";
import DBService from '../services/dbservice';
import helper from './helper';
const router = express.Router();


router.get('/', async (req, res, next) => {
    try {
        const providerList = await helper.getProviderList();
        res.status(200).send({providers:providerList});
    }
    catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;