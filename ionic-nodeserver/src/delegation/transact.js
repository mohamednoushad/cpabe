import transaction from '../transaction';
import config from '../config';

const abi = config.abi;

module.exports = {
    addNominee: async (nominee, user, user_privatekey) => {
        try {
            let params = [nominee, user];
            let sameUser = null;
            const nomineeTx = await transaction.callBlockchainTransaction("getNominees", [user], user_privatekey);
            if (nomineeTx && nomineeTx.data && Array.isArray(nomineeTx.data) && nomineeTx.data.length > 0) {
                sameUser = nomineeTx.data.find(nominee => nominee === user);
            }
            //TODO Check if the user already nominated
            if (!sameUser) {
                const tx = await transaction.callBlockchainTransaction("nominate", params, user_privatekey);
                if (tx && tx.status === 200) {
                    const nominee_tx = await transaction.callBlockchainTransaction("getNominees", [user], user_privatekey);
                    return nominee_tx && nominee_tx.status === 200 ? nominee_tx.data : null;
                }
                else {
                    return tx;
                }
            }
            else {
                return {
                    error: "401",
                    message: "Cannot nominate yourself"
                }
            }
        }
        catch (e) { return {status: 500, error:e }; }
    },

    getNominees: async(data) => {
        try {
            const tx = await transaction.callBlockchainTransaction("getNominees", [data]);
            return tx && tx.status === 200 ? tx.data : null;
        }
        catch (e) { return {status: 500, error:e }; }
    },

    getNominators: async(data) => {
        try {
            const tx = await transaction.callBlockchainTransaction("getNominators", [data]);
            return tx && tx.status === 200 ? tx.data : null;
        }
        catch (e) { return {status: 500, error:e }; }
    },

    revokeNomination : async(data, user_privatekey) => {
        try {
            const tx =  await transaction.callBlockchainTransaction("revokeNomination", data, user_privatekey);
            if (tx && tx.status === 200) {
                const nominee_tx = await transaction.callBlockchainTransaction("getNominees", [data[1]]);
                return nominee_tx && nominee_tx.status === 200 ? nominee_tx.data : null;
            }
        }
        catch (e) { return {status: 500, error:e }; }
    }
}
