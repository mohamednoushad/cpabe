import transaction from "./transact";
import DBService from '../services/dbservice';
import keythereum from '../keystore/keythereum';
import { decrypt } from '../helper';

const fs = require('promise-fs');
module.exports = {
    manage: async (data) => {
        try {
            const user_id = data.ssn,
                toAddress = data.toAddress,
                operation_type = data.type /*true - grant, false - revoke*/;

            const params = {
                UserID: user_id
            };

            const globalIdResult = await DBService.getDetails({params:params,columnName:'userID',selectColumns:['globalID'],tableName:'Correlation'});
            const globalParams = {
                globalID: globalIdResult[0].globalID
            };

            const user = await DBService.getDetails({params:globalParams,columnName:'globalID',selectColumns:['Account','otp'],tableName:'users'});

            var cipherQuery = "SELECT cipher,emrOrder FROM MultipleUUID WHERE globalID = ? AND system = ?;"
            var queryParams = [globalIdResult[0].globalID, data.emrDetails.number];
            const dbCipherAndEmrOrder = await DBService.executeQuery({query:cipherQuery,values:queryParams});

            const user_address = '0x' + user[0].Account;
            const user_privatekey = await keythereum.getPrivateKey(user_address, decrypt(dbCipherAndEmrOrder[0].cipher).password);
            const grantAccessTx = await transaction.manage(user_address, toAddress, user[0].name, user_privatekey, operation_type);
            if (grantAccessTx.error) {
                return grantAccessTx;
            }
            else {
                return grantAccessTx;
            }
        }
        catch (err) {
            return err;
        }
    }
}