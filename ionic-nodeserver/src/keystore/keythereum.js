import config from '../config';
const keythereum = require("keythereum");
const promisefs = require('promise-fs');

function importFromFile(address, data) {
    try{
        return keythereum.importFromFile(address, data);
    } catch(e){
        console.log(e);
        throw e;
    }
    
}

function recover(password, keyObject) {
    return keythereum.recover(password, keyObject);
}

function exportToFile(keyObject, datadir) {
    return keythereum.exportToFile(keyObject, datadir);
}

module.exports = {
    generateKeyObject: function (password) {
        console.log(this);
        //Creating keystore
        var params = {
            keyBytes: 32,
            ivBytes: 16
        };
        var dk = keythereum.create(params);
        var kdf = "pbkdf2";
        var options = {
            kdf: "pbkdf2",
            cipher: "aes-128-ctr",
            kdfparams: {
                c: 262144,
                dklen: 32,
                prf: "hmac-sha256"
            }
        };
        return keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);
    },
    generateKey: function (password) {
        //Creating keystore
        var params = {
            keyBytes: 32,
            ivBytes: 16
        };
        var dk = keythereum.create(params);
        var kdf = "pbkdf2";
        var options = {
            kdf: "pbkdf2",
            cipher: "aes-128-ctr",
            kdfparams: {
                c: 262144,
                dklen: 32,
                prf: "hmac-sha256"
            }
        };
        return keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);
    },
    exportToFile: (keyObject, datadir) => exportToFile(keyObject, datadir),
    recover: (password, keyObject) => recover(password, keyObject),
    importFromFile: (address, data) => importFromFile(address, data),
    getPrivateKey: async (publicKey, password) => {
        console.log(publicKey)
        const keystoreFiles = await promisefs.readdir(config.keystoreFilePath);
        if (keystoreFiles) {
            const keyObject = importFromFile(publicKey, config.keystoreFilePath.replace("/keystore", ""));
            const keyObject_privateKey = recover(password, keyObject);
            const privateKey = "0x" + keyObject_privateKey.toString('hex');
            return privateKey;
        }
        else {
            return {
                error : 404,
                message : "Keystore file unavailable"
            }
        }
    }
}