
var request = require('request');
var otplib = require('otplib');
otplib.authenticator.options = {
  step: 3000000,
  window: 1
};
var web3 = require('web3');
var sha256 = require('sha256');
var mkdirp = require('mkdirp');
var os = require('os');
const fs = require('fs');
const PATH = require('path');
var ethers = require('ethers');
const getUuidByString = require('uuid-by-string');

var config = require('./config');
const mysql = require('./mysql');
var aws = require('./aws');
var encryptionHelper = require("./encryptionHelper");
var keythereum = require('./keythereum');
var transaction = require('./transaction');
var openmrs = require('./openmrs');

var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var app = express();
const routes = require('./routes');
app.use(cors());
app.use(bodyParser.json());

const log = require(PATH.resolve('src/log.js'));
process.on('uncaughtException', function (err) {
  log.error(err);
  process.exit(1);
});

process.on('unhandledRejection', (err) => {
  log.error(err);
  process.exit(1);
});

app.use('/api', routes);
const responseParser = require(PATH.resolve('src/responseParser.js'))

let cipherKeyIv = config.cipherKeyIv;
const algorithm = encryptionHelper.CIPHERS.AES_256;

//var provider = ethers.providers.getDefaultProvider('rinkeby');


var prefix = os.homedir();
var datadir = prefix + '/mobile_wallet/doctor/keystore';

initializeServer();


function initializeServer() {
  mkdirp(datadir, function (err) {
    if (err) {
      console.error(err);
      res.status(500).send(err);
    }
    else {
      console.log('Directory Created');
      mysql.createDBConnection()
        .then(res => {
          console.log("Connected!");
          app.listen(4445, function () {
            console.log("application listening on port 4445");
          });
        })
        .catch(e => log.error(e));
    }
  });

}

function getUserWalletCredentials(params) {
  console.log("----------------getUserWalletCredentials----------------", params);
  return new Promise((resolve, reject) => {
    const sql_query = `SELECT * FROM users where Adhaar = ${mysql.escape(params.InputAdhaar)};`;
    mysql.query(sql_query).then(result => {
      console.log("query the user with the submitted adhaar number");
      if (result && result.length !== 0) {
        console.log("--Success--");
        fs.readdir(datadir, (err, keystore_files) => {
          console.log("Read the keystore folder", datadir);
          if (err) {
            console.log("getUserWallet error 1", err);
            reject(err);
          }
          else {
            const user_adress_db = result[0].Account;
            console.log("User address as obtained from DB", user_adress_db);
            const user_keystore_file = keystore_files.find(file => {
              console.log("Iterate through each keystore file to get the matching one", file);
              let file_address = file.split('--').pop();
              return file_address == user_adress_db;
            });
            console.log("------Users keystore file obtained------", user_keystore_file);
            if (user_keystore_file) {
              const keystore_file_path = datadir + '/' + user_keystore_file;
              console.log("Abosulte path of the keystore file", keystore_file_path);
              fs.readFile(keystore_file_path, (err, keystoreData) => {
                if (err) {
                  console.log("getUserWallet error 2", err);
                  reject(err);
                }
                const keyStoreObject = JSON.parse(keystoreData);
                console.log(keyStoreObject);
                var user_pk = keyStoreObject.address;
                var user_pk_hex = "0x" + user_pk;
                console.log(user_pk_hex);
                console.log("params", params);
                console.log("---------User details---------");
                console.log("keyStore Object", keyStoreObject);
                console.log("user public key HEX -->", user_pk_hex);
                console.log("---------User details End---------");
                resolve({ params, keystore: keyStoreObject, userPublicAddress: user_pk_hex });
              });
            }
          }
        })
      }

    })
      .catch(err => {
        console.log(err);
        reject(err);
      })

  })

}


function getOpenmrsData(response, id) {
  console.log("----------getOpenmrsData------------")
  var data = response.params;
  console.log("User PUB KEY", response.userPublicAddress)
  console.log("blockidentifier", id)
  var userPublicAddress = response.userPublicAddress;
  console.log("req obj", data);
  return new Promise((resolve, reject) => {
    if (data.user == 'Doctor') {
      console.log('Doctor user logged in.');
      if (data.type == 'doctordata') {
        console.log("Doctor user logged in. Perform his functionalities");
        var contract = new ethers.Contract(config.contract_address, config.abi, transaction.provider);
        var callPromise = contract.loginDoctor(userPublicAddress, web3.utils.fromAscii(id)/*, web3.utils.fromAscii(data.InputAdhaar)*/);
        console.log("Get information from doctor contract");
        callPromise
          .then(function (result) {
            console.log("Fetched data from doctor contract", result);
            var searchnumber = result[2];
            var reqdepartment = web3.utils.toUtf8(result[3]);
            var reqhospital = web3.utils.toUtf8(result[4]);
            var getPatient = {
              method: 'GET',
              url: config.OPENMRS_FHIR_BASE_URL + '/Patient',
              qs: {
                identifier: searchnumber,
              },
              headers: {
                'Cache-Control': 'no-cache',
                Authorization: 'Basic YWRtaW46QWRtaW4xMjM='
              }
            };
            request(getPatient, function (error, response, body) {
                if (error || (body.issue && body.issue.length && body.issue[0].diagnostics)) {
                    let err = error || {message: body.issue[0].diagnostics};
                    reject(err);
                }
              else {
                var modifiedbody = {
                  "openmrsbody": body,
                  "department": reqdepartment,
                  "hospital": reqhospital
                };
                console.log("Data obtained from OPENMRS system", modifiedbody);
                resolve(modifiedbody);
              }
            });
          })
          .catch(function (error) {
            console.log("error in doctordata fetch", error);
            reject(error);
          });
      } else if (data.type == 'shareaddress') {
        console.log("Get doctors public address for sharing", userPublicAddress);
        resolve({ requiredaddress: response.userPublicAddress });
      } else if (data.type == 'getpatientfromdoctor') {
        transaction.getDoctorAccessList(userPublicAddress)
          .then(doctorAccessList => {
            console.log(doctorAccessList);
            if (doctorAccessList.includes(data.patientAddress)) {
              console.log("Patient Present in Doctor's Access List");
              var patient_at_index = doctorAccessList.findIndex(item => data.patientAddress.toLowerCase() === item.toLowerCase());
              console.log(patient_at_index);
              transaction.getPatientFromDoctor(userPublicAddress, patient_at_index)
                .then(callValue => {
                  console.log(callValue);
                  var searchnumber = callValue[2];

                  if (searchnumber == '') {
                    reject({ message: "No Patient has shared the data" });
                  } else {
                    var getPatient = {
                      method: 'GET',
                      url: config.OPENMRS_FHIR_BASE_URL + '/Patient',
                      qs: {
                        identifier: searchnumber,
                      },
                      headers: {
                        'Cache-Control': 'no-cache',
                        Authorization: 'Basic YWRtaW46QWRtaW4xMjM='
                      }
                    };
                    request(getPatient, function (error, response, body) {
                        if (error || (body.issue && body.issue.length && body.issue[0].diagnostics)) {
                            let err = error || {message: body.issue[0].diagnostics};
                            console.log("error in patient data fetch for doctor-OPENMRS", error);
                            reject(err);
                        }
                      else {
                        console.log("Obtained patient information for doctor-OPENMRS", body);
                        resolve({ body });
                      }

                    });
                  }
                })
            } else {
              console.log("Patient Not Present in the Doctors List");
              reject({ message: "Patient has not shared the data" });
            }
          })
          .catch(error => {
            console.log("error in doctordata fetch - EVM", error);
            reject(error);
          });

      } else if (data.type = 'getfilefromdoctor') {
        console.log("getting patient file from doctor");
        transaction.getDoctorAccessList(userPublicAddress)
          .then(doctorAccessList => {
            console.log(doctorAccessList);
            if (doctorAccessList.includes(data.patientAddress)) {
              console.log("Patient Present in Doctor's Access List");
              var patient_at_index = doctorAccessList.findIndex(item => data.patientAddress.toLowerCase() === item.toLowerCase());
              transaction.getPatientFileFromDoctor(userPublicAddress, patient_at_index)
                .then(result => {
                  resolve({ result });
                })
            } else {
              console.log("Patient has revoked access to the doctor");
              reject({ message: "Patient has revoked access to the doctor" })
            }
          }).catch(error => {
            console.log("error in getting patient file in doctor - EVM", error);
            reject(error);
          });
      }

    } else if (data.user == 'Patient') {
      console.log("Patient logged in. Get patient info");
      var contract = new ethers.Contract(config.contract_address, config.abi, transaction.provider);
      var callPromise = contract.loginpatient(userPublicAddress, web3.utils.fromAscii(id), web3.utils.fromAscii(data.InputAdhaar));
      callPromise.then(function (result) {
        console.log("Obtained patient details for ", userPublicAddress);
        console.log("OPENMRS search number", result);
        var searchnumber = result[2];

        if (data.type == 'patientdata') {
          var getPatient = {
            method: 'GET',
            url: config.OPENMRS_FHIR_BASE_URL + '/Patient',
            qs: {
              identifier: searchnumber,
            },
            headers: {
              'Cache-Control': 'no-cache',
              Authorization: 'Basic YWRtaW46QWRtaW4xMjM='
            }
          };
          request(getPatient, function (error, response, body) {
              if (error || (body.issue && body.issue.length && body.issue[0].diagnostics)) {
                  console.log("error in fetching patient information - OPENMRS", error);
                  let err = error || {message: body.issue[0].diagnostics};
                  reject(err);
              }
            else {
              console.log("Data obtained for the patient from OPENMRS", body);
              resolve(body);
            }
          });
        } else if (data.type == 'patientvisits') {
          console.log("Get patient visit information from OPENMRS");
          var getVisit = {
            method: 'GET',
            url: config.OPENMRS_FHIR_BASE_URL + '/Encounter',
            qs: {
              identifier: searchnumber,
            },
            headers: {
              'Cache-Control': 'no-cache',
              Authorization: 'Basic YWRtaW46QWRtaW4xMjM='
            }
          };
          request(getVisit, function (error, response, body) {
              if (error || (body.issue && body.issue.length && body.issue[0].diagnostics)) {
                  console.log("error in fetching patient visits information - OPENMRS", error);
                  let err = error || {message: body.issue[0].diagnostics};
                  reject(err);
              }
            else {
              console.log("Data obtained for the patient visits from OPENMRS", body);
              resolve(body);
            }
          });
        }
      })
        .catch(error => {
          console.log("not able to execute contract", error);
          reject(error);
        });
    } else if (data.user = 'getpatientfile') {
      console.log("Get patient file for the patient");
      var contract = new ethers.Contract(config.contract_address, config.abi, transaction.provider);
      var getpatientfile = contract.gethashpatient(userPublicAddress, web3.utils.fromAscii(id));
      getpatientfile.then(function (result) {
        console.log("Patient file obtained from Ethereum", result);
        resolve({ result });
      })
        .catch(error => {
          console.log("Error in fetching patient file", error);
          reject(error);
        });
    }
  });
}



// app.post('/fetchDetails', (req, res, next) => {
//   var data = req.body;
//   const otp = data.otp;
//   const params = data.reqObj ? data.reqObj : data;
//   console.log("---------------------fetchDetails-------------------------");
//   console.log("req Obj", data);
//   if (otp) {
//     const sql_query = `SELECT * FROM users where Adhaar = ${mysql.escape(params.InputAdhaar)};`;
//     mysql.query(sql_query)
//       .then(result => {
//         if (result && result.length === 1) {
//           console.log("Validate the OTP from user if user is logging in");
//           const isValid = otplib.authenticator.check(otp, result[0].otp);
//           console.log("--------------Validated------------", isValid);
//           if (isValid) {
//             const userData = result[0];
//             const decipheredContent = encryptionHelper.decryptText(algorithm, cipherKeyIv.key, cipherKeyIv.iv, result[0].cipher, "hex");
//             let decipheredJSON = JSON.parse(decipheredContent);
//             console.log("Get user phone number from cipher", decipheredJSON);
//             let user_sk_key_payload = decipheredJSON.userName + decipheredJSON.password + decipheredJSON.phoneNumber;
//             console.log("payload to create chain identifier", decipheredJSON.userName + decipheredJSON.password + decipheredJSON.phoneNumber);
//             var blockIdentifier = getUuidByString(user_sk_key_payload, config.APP).replace(/-/g, '');
//             console.log("blockIdentifier", blockIdentifier);
//             getUserWalletCredentials(params)
//               .then(response => {
//                 console.log("obtained user wallet details---Going to get OPENMRS data", response);
//                 getOpenmrsData(response, blockIdentifier)
//                   .then(result => {
//                     //result.userPublicAddress = response.userPublicAddress;
//                     res.send({result, userData});
//                   })
//                   .catch(err => {
//                     console.log(err);
//                     res.status(404).send(err);
//                   });
//               })
//               .catch(err => {
//                 console.log(err);
//               })
//           }
//           else {

//           }
//         }
//         else {
//           getUserWalletCredentials(params)
//             .then(response => {
//               console.log(response);
//               getOpenmrsData(response)
//                 .then(result => {
//                   res.send({result, userData});
//                 })
//                 .catch(err => {
//                   console.log(err);
//                   res.status(500).send(err);
//                 })
//             })
//             .catch(err => {
//               console.log(err);
//             })
//         }
//       })
//       .catch(err => {
//         console.log(err);
//         res.status(400).send(err)
//       })
//   }
//   else {
//     const sql_query = `SELECT * FROM users where Adhaar = ${mysql.escape(params.InputAdhaar)};`;
//     console.log(sql_query);
//     mysql.query(sql_query)
//       .then(result => {
//         if (result && result.length === 1) {
//           const userData = result[0];
//           console.log("result", result[0]);
//           const decipheredContent = encryptionHelper.decryptText(algorithm, cipherKeyIv.key, cipherKeyIv.iv, result[0].cipher, "hex");
//           console.log(decipheredContent);
//           let decipheredJSON = JSON.parse(decipheredContent)
//           let user_sk_key_payload = decipheredJSON.userName + decipheredJSON.password + decipheredJSON.phoneNumber;
//           console.log("999999999999999", decipheredJSON.userName + decipheredJSON.password + decipheredJSON.phoneNumber);
//           var blockIdentifier = getUuidByString(user_sk_key_payload, config.APP).replace(/-/g, '');
//           console.log("blockIdentifier", blockIdentifier);
//           console.log(params);
//           getUserWalletCredentials(params)
//             .then(response => {
//               console.log("getopenmrs")
//               getOpenmrsData(response, blockIdentifier)
//                 .then(result => {
//                   res.send({result, userData});
//                 })
//                 .catch(err => {
//                   console.log(err);
//                   res.status(400).send(err);
//                 })
//             })
//             .catch(err => {
//               console.log(err);
//             })
//         }
//         else {
//           getUserWalletCredentials(params)
//             .then(response => {
//               console.log(response);
//               getOpenmrsData(response)
//                 .then(result => {
//                   res.send({result, userData});
//                 })
//                 .catch(err => {
//                   console.log(err);
//                   res.status(500).send(err);
//                 })
//             })
//             .catch(err => {
//               console.log(err);
//             })
//         }
//       })
//       .catch(err => {
//         console.log(err);
//         res.status(400).send(err)
//       })
//   }
// });

app.post('/getSharedPatientList', function (req, res) {
  var data = req.body;
  const params = data.reqObj ? data.reqObj : data;
  const sql_query = `SELECT * FROM users where Adhaar = ${mysql.escape(params.InputAdhaar)};`;
  console.log(sql_query);
  mysql.query(sql_query)
    .then(result => {
      console.log("result", result[0]);
      var doctorPubKey = result[0].Account;
      var doctor_PubKey_hex = '0x' + doctorPubKey;
      transaction.getDoctorAccessList(doctor_PubKey_hex)
        .then(async (accessList) => {
          console.log(accessList);
          // res.send({accesslist:accessList});

          if (accessList.includes('0x0000000000000000000000000000000000000000')) {
            console.log("0x00 present");
            var myArray = new Set(accessList);
            console.log(myArray);
            myArray.delete('0x0000000000000000000000000000000000000000');
            var needArray = Array.from(myArray);
            console.log(needArray);
            console.log({ accesslist: needArray });


          } else {
            var myArray = new Set(accessList);
            console.log(myArray);
            var needArray = Array.from(myArray);
            console.log(needArray);
            console.log({ accesslist: needArray });


          }
          let usersResult = [];
          if (needArray.length) {
            var searchArray = [];
            for (var i of needArray) {
              var pk_without_hex = i.slice(2);
              searchArray.push(pk_without_hex.toLowerCase());
            }
            let usersQuery = getUserFullNameQuery(searchArray);
            console.log(usersQuery);
            usersResult = await mysql.query(usersQuery, []);
            console.log(usersResult);
          }
          res.send({ accesslist: needArray, userList: usersResult });







        }).catch(err => {
          console.log(err);
          res.status(400).send(err);
        })
    });
});

function getUserFullNameQuery(userAddress) {
  let addresses = '';

  addresses += ' (';
  for (let item of userAddress) {
    addresses += `'${item}'`;
    (userAddress.indexOf(item) < userAddress.length - 1) ? addresses += ',' : addresses += ')';
  }

  return `SELECT Account,name FROM users WHERE lower(Account) IN ${addresses};`;
}


app.get('/getPatientVisits', (req, res, next) => {
  var query = req.query;
  var getVisit = {
    method: 'GET',
    url: config.OPENMRS_FHIR_BASE_URL + '/Encounter',
    qs: {
      identifier: query.identifier,
    },
    headers: {
      'Cache-Control': 'no-cache',
      Authorization: 'Basic YWRtaW46QWRtaW4xMjM='
    }
  };
  request(getVisit, function (error, response, body) {
    if (error || (body.issue && body.issue.length && body.issue[0].diagnostics)) {
      console.log("error in fetching patient visits information - OPENMRS", error);
        let err = error || {message: body.issue[0].diagnostics};
        reject(err);
    }
    else {
      console.log("Data obtained for the patient visits from OPENMRS", body);
      res.send(body);
    }
  });
});

app.get('/getVisitDetails', (req, res, next) => {
  const query = req.query;
  console.log(query.subject);
  const requestObj = {
    method: 'GET',
    url: config.OPENMRS_FHIR_BASE_URL + '/Observation',
    qs: {
      subject: query.subject,
    },
    headers: {
      'Cache-Control': 'no-cache',
      Authorization: 'Basic YWRtaW46QWRtaW4xMjM='
    }
  };
  request(requestObj, function (error, response, body) {
      if (error || (body.issue && body.issue.length && body.issue[0].diagnostics)) {
          let err = error || {message: body.issue[0].diagnostics};
          throw new Error(err);
      }
    else {
      console.log("Data obtained from OPENMRS system", body);
      res.send(body);
    }
  });

});

app.get('/prescriptions', async (req, res, next) => {
    try {
        let response = await openmrs.getPrescriptions(req.query.personId, +req.query.provider);
        res.send(response);
    }catch(err){
        res.status(500).send(err);
    }
});
