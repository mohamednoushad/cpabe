import express from 'express';
import PATH from 'path';
const router = express.Router();
const userRouter = require(PATH.resolve('src/user'));
const notificationController = require(PATH.resolve('src/notifications')).controller;
const delegationRouter = require(PATH.resolve('src/delegation'));
const ownershipRouter = require(PATH.resolve('src/ownership'));
const providers = require(PATH.resolve('src/providers'))
const history = require(PATH.resolve('src/history'))
router.use('/notifications', notificationController);
router.use('/delegate', delegationRouter);
router.use('/ownership', ownershipRouter);
router.use('/user', userRouter);
router.use('/providers',providers);
router.use('/history',history);
module.exports = router;
