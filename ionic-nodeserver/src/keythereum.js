const keythereum = require("keythereum");

module.exports = {
    generateKeyObject: function (password) {
        //Creating keystore
        var params = {
            keyBytes: 32,
            ivBytes: 16
        };
        var dk = keythereum.create(params);
        var kdf = "pbkdf2";
        var options = {
            kdf: "pbkdf2",
            cipher: "aes-128-ctr",
            kdfparams: {
                c: 262144,
                dklen: 32,
                prf: "hmac-sha256"
            }
        };
        return keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);
    },
    exportToFile: (keyObject, datadir) => keythereum.exportToFile(keyObject, datadir),
    recover: (password, keyObject) => keythereum.recover(password, keyObject),
    importFromFile : (address,data) => keythereum.importFromFile(address, data)
}