import mysql from '../mysql';

module.exports = {

    getDetails : ({params, columnName ,dialect, selectColumns,tableName}) => {

        let query_params = [];
        const select_columns = (selectColumns && selectColumns.length) ? selectColumns.join(' , ') : "*";
        console.log(select_columns);
        if(Array.isArray(params)) {
            for (var value of params) {
                query_params.push(`${columnName} = '${value.toLowerCase().substr(2)}'`);
            }
        }
        else {
            for (var key in params) {
                query_params.push(`${key} = "${params[key]}"`);
            }
        }
        query_params = query_params.join(` ${dialect || ','} `);
        const query = `SELECT ${select_columns} FROM ${tableName} WHERE ${query_params}`;
        console.log(query);
        return mysql.query(query);
    },

    getUserDetails : (params, columnName ,dialect, selectColumns) => {
        let query_params = [];
        const select_columns = (selectColumns && selectColumns.length) ? selectColumns.join(' , ') : "*";
        if(Array.isArray(params)) {
            for (var value of params) {
                query_params.push(`${columnName} = '${value.toLowerCase().substr(2)}'`);
            }
        }
        else {
            for (var key in params) {
                query_params.push(`${key} = "${params[key]}"`);
            }
        }
        query_params = query_params.join(` ${dialect || ','} `);
        const query = `SELECT ${select_columns} FROM users WHERE ${query_params}`;
        console.log(query);
        return mysql.query(query);
    },

    // params {Object}
    //      keys --- table : tablename ,
    //      columns --- columns that has to be updated,
    //      values --- values for the columns Array of values
    insertQuery : (params, isMultipleInsert) => {
        const table = params.table,
            columns = params.columns,
            values = params.values,
            query = `INSERT INTO ${table}(${columns.join(',')}) VALUES ?`;
        return mysql.query(query, [values]);
    },

    updateQuery: (params, dialect) => {
        let set_params = [],
            where_params = [];
        const table = params.table,
            setColumns = params.set,
            whereColumns = params.where;
        for (var key in setColumns) {
            set_params.push(`${key} = "${setColumns[key]}"`);
        }
        for (var key in whereColumns) {
            where_params.push(`${key} = "${whereColumns[key]}"`);
        }
        let condition = '';
        if(!Array.isArray(dialect)) {
            condition = where_params.join(' ' + dialect + ' ');
        }
        else if(dialect.length === where_params.length - 1) {
            dialect.forEach((d,i) => {
                if(condition) condition = condition + " " + d + " " + where_params[i+1];
                else condition = " " + where_params[i] + " " + d + " " + where_params[i+1];
            });
        }
        else {
            condition = where_params[0];
        }
        const query = `UPDATE ${table} SET ${set_params.join(',')} WHERE ${condition}`;
        console.log("query", query);
        return mysql.query(query);
    },

    matchItemQuery: (params) => {
        let columns = params && params.columns && params.columns.length ? params.columns.join(' , '): "*";
        const query = `SELECT ${columns} FROM ${params.table} WHERE ${params.where} IN ${params.value};`;
        console.log(query);
        return mysql.query(query);
    },

     getAllRecordsFromTable :({tableName}) => {
        const query = `SELECT * FROM ${tableName};`;
        console.log(query);
        return mysql.query(query);
    },

    executeQuery : ({query, values}) => {
        return mysql.query(query, values);
    }
}
