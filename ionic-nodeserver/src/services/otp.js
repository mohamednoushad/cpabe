const otplib = require('otplib');
otplib.authenticator.options = {
  step: 3000000,
  window: 1
};

module.exports = {
    generateSecret: () => otplib.authenticator.generateSecret(),
    generateToken: (secret) => otplib.authenticator.generate(secret),
    validateOTP: (otp, secret) => otplib.authenticator.check(otp, secret) 
}