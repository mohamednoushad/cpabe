module.exports = {
    ERROR: {
        SSN_INVALID : {
            code:'SSN_INVALID',
            text:'Invalid SSN'
        },
        NO_PATIENT_DATA_AVAILABLE : {
            code:'NO_PATIENT_DATA_AVAILABLE',
            text:'No Patient Data Available'
        }
    },
    SUCCESS: {
        ACCESS_GRANTED : {
            code: 'ACCESS_GRANTED',
            text: 'Access Granted'
        },
        ACCESS_CANCELLED : {
            code: 'ACCESS_CANCELLED',
            text: 'Access cancelled'
        },
        FILE_UPLOADED_SUCCESSFULLY : {
            code: 'FILE_UPLOADED_SUCCESSFULLY',
            text: 'File Uploaded Successfully'
        },
        NOTIFICATIONS_READ_SUCCESS : {
            code: 'NOTIFICATIONS_READ_SUCCESS',
            text: 'Notifications marked as read'
        }
    }
}
