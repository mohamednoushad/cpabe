const request = require("request");
const request_promise = require("request-promise-native");
const config = require('./config');
const OPENMRS_URL = config.OPENMRS_FHIR_BASE_URL + '/Patient';
const OPENMRS_ENCOUNTER = config.OPENMRS_FHIR_BASE_URL + '/Encounter';


function requestOpenMRS(payload) {
    let req;
    const { method, url, query, body, department, hospital } = payload;
    switch (method) {
        case "GET":
            req = { method, url, qs: query }
            break;
        case "POST":
            req = { method, url, body, json: true }
            break;
        case "PATCH":
            req = { method, url, body, json: true }
            break;
        case "PUT":
            req = { method, url, body, json: true }
            break;
    }
    req.headers = {
        'Cache-Control': 'no-cache',
        Authorization: 'Basic YWRtaW46QWRtaW4xMjM=',
        'Content-Type': 'application/json+fhir'
    };
    console.log(req);
    return new Promise((resolve, reject) => {
        request(req, function (error, response, body) {
            if (error || (body && body.issue && body.issue.length && body.issue[0].diagnostics)) {
                let err = error || { message: body.issue[0].diagnostics };
                reject(err);
            }
            else {
                console.log("Data obtained  from emr", body);
                if(payload.encapsulate){
                    resolve({body, department, hospital});
                }
                else {
                    resolve(body);
                }
            }
        });
    });
}

module.exports = {
    addUser: async (data, providerId) => {
        let body = {
            "resourceType": "Patient",
            id: data.emrId,
            "identifier": [{
                "use": "usual",
                "system": "Old Identification Number",
                "value": data.emrId
            }],
            "name": [{
                "use": "usual",
                "family": [data.familyName],
                "given": [data.firstName],
                "prefix": ["Mr."],
                "suffix": ["Mr."]
            }],
            "gender": data.gender,
            "birthDate": data.dob.split('T')[0],
            "deceasedBoolean": false,
            "address": [{
                "use": "home",
                "type": "postal",
                "line": [data.addressLine1, data.addressLine2],
                "text": data.addressLine1 + ', ' + data.addressLine2,
                "city": data.city,
                "state": data.state,
                "postalCode": data.areaCode,
                "country": data.country
            }],
            "active": true
        };
        let providerbaseUrl = config.getEmrUrl(providerId);

        const payload = {
            method: 'POST',
            url: providerbaseUrl + '/Patient',
            body,
        }
        return requestOpenMRS(payload);
    },

    patchUser: async (data, providerId) => {
        console.log("calling Patch");
        console.log(data,providerId);
        let body = {
            "resourceType": "Patient",
            id: data.emrId
        };
        let providerbaseUrl = config.getEmrUrl(providerId);

        const payload = {
            method: 'PATCH',
            url: providerbaseUrl + '/Patient/' + data.existingEmrPatientId,
            body,
        }
        console.log(payload);
        return requestOpenMRS(payload);
    },

    getUserProfile: function ({data,user,emr}) {
        let emrIdentifier;
        let department;
        let hospital;
        if(Array.isArray(data)){
             emrIdentifier = data[0];
             department = data[1];
             hospital = data[2];
        }else{
            emrIdentifier = data;
        }

       var fhirUrl= config.getEmrUrl(parseInt(emr.number));

       const payload = {
            method: 'GET',
            url: fhirUrl + '/Patient',
            query: {
                identifier: emrIdentifier,
            },
            department, hospital, encapsulate : true
        };
        return requestOpenMRS(payload);

    },

    getPrescriptions: function (personId, provider) {
        return new Promise(async (resolve, reject) => {
            let providerbaseUrl = config.getEmrUrl(provider);
            const payload = {
                method: 'GET',
                url: providerbaseUrl + '/MedicationRequest',
                query: {
                    patient: `Patient/${personId}`,
                }
            };
            try {
                let data = await requestOpenMRS(payload);
                console.log(data);
                resolve(data);
            }
            catch (err) {
                console.log(err);
                reject(err);
            }
        });
    },

    getVisits: (identifier, provider) => {
        let providerbaseUrl = config.getEmrUrl(provider);
        const payload = {
            method: 'GET',
            url: providerbaseUrl + '/Encounter',
            query: {
                identifier: identifier
            }
        };
        return requestOpenMRS(payload);
    },

    getObservations: (subject, provider) => {
        let providerbaseUrl = config.getEmrUrl(provider);
        const payload = {
            method: 'GET',
            url: providerbaseUrl + '/Observation',
            query: { subject: `Patient/${subject}` }
        };
        return requestOpenMRS(payload);
    },

    getImagingStudyForPatient: (patientId, provider) => {
        let providerbaseUrl = config.getEmrUrl(provider);
        const payload = {
            method: 'GET',
            url: providerbaseUrl + '/ImagingStudy',
            query: { patient: `Patient/${patientId}` }
        };
        return requestOpenMRS(payload);
    },

    isUserPresent : (provider,emrId) => {

          
        let providerbaseUrl = config.getEmrUrl(+(provider));

        const payload = {
            method: 'GET',
            url: providerbaseUrl + '/Patient',
            query: {
                identifier: emrId,
            }
        };
     
        console.log(payload);
        return requestOpenMRS(payload);
      
    },

    updateUserProfile : async (body, providerId, patientId) => {

        try {

        let providerbaseUrl = config.getEmrUrl(providerId);

        const payload = {
            method : 'PUT',
            url : providerbaseUrl + '/Patient/' +patientId,
            body
        }

        const result =  await requestOpenMRS(payload); 

        if (!result) {  //if success , result is undefined from FHIR Layer
            return "success";
        }

    } catch (e) {
        throw e;
    }

    }

}
