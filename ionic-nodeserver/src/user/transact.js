import transaction from '../transaction';
import Web3 from 'web3';

let web3 = new Web3();

module.exports = {

    attachFiletoUser: async (encryptedId, url, userPrivateKey) => {
        try {
            const params = [web3.fromAscii(encryptedId), url];
            return await transaction.callBlockchainTransaction("addfilepatient", params, userPrivateKey);
        }
        catch (e) { throw e; }
    },

    transferEthers: async (masterPrivateKey, account) => {
        console.log("calling transfer ether function",masterPrivateKey,account);
        try {
            return await transaction.transferEthers(masterPrivateKey, account);
        }
        catch (e) { throw e; }
    },

    addUser: async (data, identifier, userAddress, userPrivateKey) => {
        let params = [];
        let tx;
        try {
            switch (data.user) {
                case "doctor":
                    params = [userAddress, web3.fromAscii(data.globalId), web3.fromAscii(identifier), data.emrId, data.department, data.hospital];
                    tx = await transaction.callBlockchainTransaction("insertDoctor", params, userPrivateKey);
                    break;
                case "patient":
                    params = [userAddress, web3.fromAscii(data.globalId), web3.fromAscii(identifier), data.emrId];
                    tx = await transaction.callBlockchainTransaction("insertPatient", params, userPrivateKey);
                    break;
                default:
                    console.log("default");
                    break;
            }
            return tx;
        }
        catch (e) { throw e; }
    },

    authenticateUser: async (address, encryptedIdentifier, globalId, emrOrder, user) => {
        let params = [];
        let tx;
        try {
            switch (user) {
                case "patient":
                    console.log(address, globalId, encryptedIdentifier,parseInt(emrOrder)-1);
                    params = [address, web3.fromAscii(globalId), web3.fromAscii(encryptedIdentifier),parseInt(emrOrder)-1];
                    console.log(params);
                    tx = await transaction.callBlockchainTransaction("loginpatient", params);
                    break;
                case "doctor":
                    console.log(parseInt(emrOrder)-1);
                    params = [address, web3.fromAscii(encryptedIdentifier),web3.fromAscii(globalId),parseInt(emrOrder)-1]; // parseInt(emrOrder)-1 to call the index of array from blockchain
                    tx = await transaction.callBlockchainTransaction("loginDoctor", params);
                    break;
                default:
                    break;
            }
            return tx;
        }
        catch (e) {
            throw e;
        }
    },

    getAllocatedUsers: async (address) => {
        try {
            return await transaction.callBlockchainTransaction("getGivenAccesslist", [address]);
        }
        catch (e) {
            throw e;
        }
    },

    getAllocatedDoctors: async (address) => {
        try {
            return await transaction.callBlockchainTransaction("getPatientSharedList", [address]);
        }
        catch (e) {
            throw e;
        }
    },

    getGrantorBlockId: async (recipient, grantorIndex, patientIndex) => {
        try {
            return await transaction.callBlockchainTransaction("getPatient", [recipient, grantorIndex,patientIndex]);
        }
        catch (e) {
            throw e;
        }
    },

    getFile: async (user, data) => {
        try {
            if(user.toLowerCase() === "patient") {
                const params = [data.account, web3.fromAscii(data.encryptedId),data.index];
                return await transaction.callBlockchainTransaction("getfilepatient", params);
            }
            else if (user.toLowerCase() === "doctor") {
                const params = [data.recipient, data.grantorIndex, data.patientEmrIndex];
                return await transaction.callBlockchainTransaction("getPatientFileFromdoctor", params);
            }
            else {
                throw ("Invalid request")
            }
        }
        catch (e) {throw e;}
    }

}

