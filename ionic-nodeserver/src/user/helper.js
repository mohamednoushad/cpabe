import otp from '../services/otp';
import DBService from '../services/dbservice';
import { decrypt, encrypt, uuid } from '../helper';
import aws from '../aws';
import config from "../config";
import userRepo from "./userRepo";

module.exports = {
    generateRegistrationOTP: async (data) => {
        try {
            if (!data || !data.password || !data.ssn || !data.phoneNumber){
                throw new Error("request malformed");
            }

            let emrProvider = data.provider || config.healthereumProviderId;
            let {isValid, hasCorrelationMapping, globalId, hasAccount, hasPreviousEntryInUsersTable} =  await validateUserRegistration(data.ssn, emrProvider);
            if(!isValid){
                throw new Error('User has already registered in the emr');
            }

            if(!hasCorrelationMapping){
                globalId = uuid().replace(/-/g, '');
                await userRepo.insertCorrelationMapping(data.ssn, globalId);
            }

            const secret = otp.generateSecret();
            if(!hasAccount && !hasPreviousEntryInUsersTable) {
                const fullName = `${data.firstName} ${data.familyName}`;
                await userRepo.insertUser(globalId, secret, fullName);
            } else{
                await userRepo.updateUserOtp(globalId, secret);
            }

            const token = otp.generateToken(secret);
            const otpMessageBody = {
                Message: `OTP for registering to Healthereum is ${token}. Please do not share your OTP with anyone.`,
                PhoneNumber: data.phoneNumber
            };
            await aws.sendOTP(otpMessageBody);

            return globalId;
        }
        catch (e) {
            throw e;
        }
    },

    generateLoginOTP: async (data) => {
        try {
            if (!data || !data.password || !data.ssn || !data.userName) {
                throw (new Error("request malformed"));
            }
            const params = {
                UserID: data.ssn
            };
            const globalIdResult = await DBService.getDetails({params:params,columnName:'userID',selectColumns:['globalID'],tableName:'Correlation'});
            const globalParams = {
                globalID: globalIdResult[0].globalID
            };
            const user = await DBService.getDetails({params:globalParams,columnName:'globalID',selectColumns:['cipher'],tableName:'MultipleUUID'});
            const userCredentials = decrypt(user[0].cipher);
            console.log(userCredentials);
            if (!userCredentials || userCredentials.password !== data.password
                || userCredentials.userName !== data.userName) {
                throw new Error("User details in the database does not match with the entered information");
            }
            else {
                console.log("proceed to OTP generaton");
                const secret = otp.generateSecret();
                const params = {
                    table : "users",
                    set : {
                        otp: secret
                    },
                    where : {
                        globalID: globalIdResult[0].globalID,
                    }

                }
                const updateUser = await DBService.updateQuery(params,"AND");
                if (updateUser.changedRows === 1 && updateUser.affectedRows === 1) {
                    const token = otp.generateToken(secret);
                    const otpMessageBody = {
                        Message: `OTP for login to Healthereum is ${token}. Please do not share your OTP with anyone.`,
                        PhoneNumber: userCredentials.phoneNumber
                    };
                    console.log("otpMessageBody", otpMessageBody);
                    return await aws.sendOTP(otpMessageBody);
                }
                else {
                    throw new Error("multiple rows updated")
                }
            }
        }
        catch (e) {
            throw e;
        }
    },

    providerListBySsn : async(data) => {
       try {
        const params = {
            UserID: data.userId
        };
        const globalIdResult = await DBService.getDetails({params:params,columnName:'userID',selectColumns:['globalID'],tableName:'Correlation'});
        console.log(globalIdResult);
        var providerQuery = "SELECT providerNumber, providerName FROM providerTable AS provider JOIN MultipleUUID AS uuid ON uuid.system = provider.providerNumber where uuid.globalID = ? ORDER BY uuid.system;";
        var queryParams = [globalIdResult[0].globalID];
        const providerList = await DBService.executeQuery({query:providerQuery,values:queryParams});
        return providerList;
       } catch(e) {
           throw e;
       }
    },

    providerListByAccount : async(data) => {
        console.log("calling provider list by accounts");
        let queryAddress = data.address.substr(2);
        console.log(queryAddress);

        try {
            const params = {
                Account: queryAddress
            };
            const globalIdResult = await DBService.getDetails({params:params,columnName:'Account',selectColumns:['globalID'],tableName:'users'});
            console.log(globalIdResult);
            //working here
            var providerQuery = "SELECT providerNumber, providerName FROM providerTable AS provider JOIN MultipleUUID AS uuid ON uuid.system = provider.providerNumber where uuid.globalID = ? ORDER BY uuid.system;";
            var queryParams = [globalIdResult[0].globalID];
            const providerList = await DBService.executeQuery({query:providerQuery,values:queryParams});
            return providerList;
        } catch(e) {
            throw e;
        }
    },

    patientProviderIndex : async({grantorAddress,systemNo}) => {

        let queryAddress = grantorAddress.substr(2);

        try {

            const params = {
                Account: queryAddress
            };
            const globalIdResult = await DBService.getDetails({params:params,columnName:'Account',selectColumns:['globalID'],tableName:'users'});

            const globalParams = {
                   globalID: globalIdResult[0].globalID
                };

            var cipherQuery = "SELECT emrOrder FROM MultipleUUID WHERE globalID = ? AND system = ?;"
            var queryParams = [globalIdResult[0].globalID, systemNo];
            const emrOrderResult = await DBService.executeQuery({query:cipherQuery,values:queryParams});
            return parseInt(emrOrderResult[0].emrOrder)-1;  //its alway one less than the system number in blockchain as first input is saved to index 0

        } catch(e) {
            throw e;
        }
    },

    validateOtp: async (ssn, otpReceived) =>{
        let globalIdResult = await userRepo.getGlobalId(ssn);
        const user = await userRepo.getUserDetails(globalIdResult.globalID);
        const isOTPValid = otp.validateOTP(otpReceived, user.otp);
        return isOTPValid;
    },

    validateUserRegistration

};

async function validateUserRegistration (ssn, provider) {
    try{
        // return invalid if user has already registered for the emr
        let isValid = true;
        let globalId;
        let userHasAccount = false;
        let hasCorrelationMapping = false;
        let accountAddress;
        let hasPreviousEntryInUsersTable = false;
        let globalIdResult = await userRepo.getGlobalId(ssn);
        if(globalIdResult){
            hasCorrelationMapping = true;
            globalId = globalIdResult.globalID;

            let accountIdResult = await userRepo.getAccountAddress(globalId);
            if(accountIdResult){
               hasPreviousEntryInUsersTable = true;
            }
            userHasAccount = accountIdResult && accountIdResult.Account;
            if(userHasAccount) {
                accountAddress = accountIdResult.Account;
                let emrUUIDResult = await userRepo.getEmrUniqueId(globalId, provider);
                let userHasRegisteredForEmr = emrUUIDResult && emrUUIDResult.emrUniqueId;

                if (userHasRegisteredForEmr) {
                    isValid = false;
                }
            }
        }
        return {
            isValid : isValid,
            hasCorrelationMapping: hasCorrelationMapping,
            globalId: globalId,
            hasAccount: userHasAccount,
            accountAddress: accountAddress,
            hasPreviousEntryInUsersTable: hasPreviousEntryInUsersTable};
    }catch(err){
        throw err;
    }
}
