import transaction from "./transact";
import DBService from '../services/dbservice';
import otp from '../services/otp';
import keythereum from '../keystore/keythereum';
import { decrypt, encrypt } from '../helper';
import sha1 from 'sha1';
import openmrs from '../openmrs';
import config from '../config';
import helper from './helper';
const getUuidByString = require('uuid-by-string');
import userRepo from "./userRepo";

module.exports = {
    attachFiletoUser: async (data) => {
        try {
            const user_id = data.ssn,
                file = data.fileUrl;

            const user = await userRepo.getUserWithSSN(user_id);

            const user_address = '0x' + user.account;
            let encryptedId = await userRepo.getEmrUniqueId(user.globalID, 1);
            const user_privatekey = await keythereum.getPrivateKey(user_address, decrypt(encryptedId.emrUniqueId).password);

            const userCredentials = decrypt(encryptedId.emrUniqueId);
            const encryptedIdentifier = getUuidByString(userCredentials.userName + userCredentials.password + userCredentials.phoneNumber + userCredentials.providerNumber, config.APP).replace(/-/g, '');

            return await transaction.attachFiletoUser(encryptedIdentifier, file, user_privatekey);
        }
        catch (e) { throw e; }
    },

    generateotp: async (data) => {
            try {
                if (data.isRegistered) {
                    return await helper.generateLoginOTP(data);
                }
                else {
                    return await helper.generateRegistrationOTP(data);
                }
            } catch (err) {
                throw err;
            }
    },

    register: async (data) => {
        try {
            if (!data || !data.password || !data.user || !data.otp || !data.ssn){
                throw new Error("request malformed");
            }

            let emrProvider = data.provider || config.healthereumProviderId;
            let {isValid, globalId, hasAccount, accountAddress} =  await helper.validateUserRegistration(data.ssn, emrProvider);
            if(!isValid){
                throw new Error('User has already registered in the emr');
            }

            const isOTPValid = await helper.validateOtp(data.ssn, data.otp);
            if (!isOTPValid) {
                throw new Error("Invalid OTP");
            }
 
            const encryptParams = {
                userName: data.userName,
                password: data.password,
                phoneNumber: data.phoneNumber,
                providerNumber: emrProvider
            };
            const encryptedContent = encrypt(JSON.stringify(encryptParams));
            await userRepo.insertEmrUserMapping(globalId, emrProvider, encryptedContent);

            let keystore;
            if(!hasAccount){
                keystore = keythereum.generateKey(data.password);
                await userRepo.updateUserAccount(globalId, keystore.address);
                keythereum.exportToFile(keystore, config.keystoreFilePath);

                await transaction.transferEthers(config.admin_pk, "0x" + keystore.address);
            }
            else{
                let keyStorePath = config.keystoreFilePath.replace("/keystore", "");
                keystore = keythereum.importFromFile(accountAddress, keyStorePath);
            }
            data.emrId = sha1(data.userName + Date.now());
            let userData = {
                globalId: globalId,
                emrId: data.emrId,
                hospital: data.hospital, // for doctor
                department: data.department,// for doctor
                user: data.user
            };
            const blockIdentifier = getUuidByString(encryptParams.userName + encryptParams.password + encryptParams.phoneNumber + encryptParams.providerNumber, config.APP).replace(/-/g, '');
            const userPrivateKey = "0x" + keythereum.recover(data.password, keystore).toString('hex');
            await transaction.addUser(userData, blockIdentifier, "0x" + keystore.address, userPrivateKey);
     

            if(data.existingEmrUser) {
                await openmrs.patchUser(data,emrProvider);
            }else{
                await openmrs.addUser(data, emrProvider);
            }

            return globalId;

        } catch (err) {
            throw err;
        }
    },

    login: async (data) => {
        console.log(data);
        try {
            console.log("login and get details");
            const params = {
                UserID: data.ssn
            };
            const globalIdResult = await DBService.getDetails({params:params,columnName:'userID',selectColumns:['globalID'],tableName:'Correlation'});
            const globalParams = {
                globalID: globalIdResult[0].globalID
            };
            const user = await DBService.getDetails({params:globalParams,columnName:'globalID',selectColumns:['Account','otp'],tableName:'users'});
            console.log(user);
            const isOtpValid = otp.validateOTP(data.otp, user[0].otp);
            if (true || isOtpValid) {
                console.log("otp is valid");
                var cipherQuery = "SELECT cipher,emrOrder FROM MultipleUUID WHERE globalID = ? AND system = ?;"
                var queryParams = [globalIdResult[0].globalID, data.emrDetails.number];
                const dbCipherAndEmrOrder = await DBService.executeQuery({query:cipherQuery,values:queryParams});
                const userCredentials = decrypt(dbCipherAndEmrOrder[0].cipher);
                if (!userCredentials || !userCredentials.userName || !userCredentials.password || !userCredentials.phoneNumber) throw new Error({ message: "Invalid user credentials." })
                const encyptedIdentifier = getUuidByString(userCredentials.userName + userCredentials.password + userCredentials.phoneNumber + userCredentials.providerNumber, config.APP).replace(/-/g, '');
                const keyObject = keythereum.importFromFile(user[0].Account, config.keystoreFilePath.replace("/keystore", ""));
                console.log("keyObject", keyObject);
                if (!keyObject || !keyObject.address){
                    throw new Error({ message: "keystore not available or malformed" });
                }
                const authenticateUserTx = await transaction.authenticateUser("0x" + keyObject.address, encyptedIdentifier, globalIdResult[0].globalID,dbCipherAndEmrOrder[0].emrOrder, data.user);
                if (authenticateUserTx && authenticateUserTx.data) {
                    if ((data.user === "patient" && [authenticateUserTx.data].length === 1) ||
                        (data.user === "doctor" && authenticateUserTx.data.length === 3)) {
                        const emrData = await openmrs.getUserProfile({data:authenticateUserTx.data, user:data.user,emr:data.emrDetails});
                        return { clinicalData: emrData, user: { account: "0x" + user[0].Account, name: user[0].name } };
                    }
                    else {
                        throw new Error({ message: "Invalid data fetched from blockchain" });
                    }
                }
                else {
                    throw new Error({ message: "Invalid data fetched from blockchain" })
                }
            }
            else {
                throw new Error({ message: "Incorrect OTP." })
            }
        }
        catch (e) {
            throw e;
        }


    },

    getVisits: (data) => {
        try {
            if (!data || !data.identifier) throw new Error("Request malformed. Patient Id required");
            return openmrs.getVisits(data.identifier, +data.provider);
        }
        catch (e) {
            throw e;
        }
    },

    getObservations: (data) => {
        try {
            if (!data || !data.subject) throw new Error("Request malformed. Subject required");
            return openmrs.getObservations(data.subject, +data.provider);
        }
        catch (e) {
            throw e;
        }
    },

    getImagingStudyForPatient: (userId, provider) => {
        try {
            if (!userId) throw new Error("Request malformed. Subject required");
            return openmrs.getImagingStudyForPatient(userId, +provider);
        }
        catch (e) {
            throw e;
        }
    },

    getAllocatedUsers: async (data) => {
        try {
            if (!data || !data.userId) throw new Error("Request malformed. Patient Id required");
            const params = {
                UserID: data.userId
            };
            const globalIdResult = await DBService.getDetails({params:params,columnName:'userID',selectColumns:['globalID'],tableName:'Correlation'});
            const globalParams = {
                globalID: globalIdResult[0].globalID
            };
            const user = await DBService.getDetails({params:globalParams,columnName:'globalID',selectColumns:['Account','otp'],tableName:'users'});
            console.log(user);
            console.log(user[0]);
            let userAddress;
            if(data.user !== "doctor"){
                userAddress = await transaction.getAllocatedUsers("0x" + user[0].Account);
            }
            else {
                userAddress = await transaction.getAllocatedDoctors("0x" + user[0].Account);
            }

            console.log(userAddress);
            if (!userAddress || !userAddress.data || userAddress.data.length === 0) {
                return { status: 200, message: "Patient data unavailable" };
            }
            else {
                let addresses = '';
                addresses += ' (';
                for (let item of userAddress.data) {
                    addresses += `'${item.slice(2)}'`;
                    (userAddress.data.indexOf(item) < userAddress.data.length - 1) ? addresses += ',' : addresses += ')';
                }
                console.log(addresses);
                const params = {
                    table: "users",
                    columns: ["Account", "name"],
                    value: addresses,
                    where: ["lower(Account)"],
                    operator: "IN"
                }
                return await DBService.matchItemQuery(params);
            }
        }
        catch (e) {
            throw e;
        }
    },

    getGrantors: async (data) => {
        console.log(data);
        if (!data || !data.recipient || !data.grantor || !data.providerNo) {
            throw ("Invalid request. Please provide addresses")
        }
        const grantorAddress = await transaction.getAllocatedUsers(data.recipient);
        console.log("userAddress", grantorAddress.data);
        const grantorIndex = grantorAddress.data.findIndex(item => data.grantor.toLowerCase() === item.toLowerCase());
        if (grantorIndex !== -1) {
            const patientIndex = await helper.patientProviderIndex({grantorAddress:data.grantor,systemNo:data.providerNo});
            const grantorTx = await transaction.getGrantorBlockId(data.recipient, grantorIndex, patientIndex);
            const grantor = await openmrs.getUserProfile({data:grantorTx.data,emr:{number:data.providerNo}});
            return grantor
        }
        else {
            throw ("Patient mapping not available against the doctor");
        }
    },

    getFileFromPatient: async(data) => {
    try {
        let globalIdResult = await userRepo.getUserWithSSN(data.ssn);
        let encryptedId = await userRepo.getEmrUniqueId(globalIdResult.globalID, data.provider);

        const userCredentials = decrypt(encryptedId.emrUniqueId);
        const encryptedIdentifier = getUuidByString(userCredentials.userName + userCredentials.password + userCredentials.phoneNumber + userCredentials.providerNumber, config.APP).replace(/-/g, '');

        let patientIndex = parseInt(encryptedId.emrOrder) - 1;
        const params = {
            account: data.account,
            encryptedId: encryptedIdentifier,
            index: patientIndex
        };
        const getFileTx = await transaction.getFile(data.user, params);
        return getFileTx;
    }catch(err){
        throw err;
    }

    },

    getPatientFileFromDoctor : async(data) => {
        try {
            if(!data || (data && (!data.recipient || !data.grantor))){
                throw "Request malformed. Please provide user address and ssn";
            }


            const grantorAddress = await transaction.getAllocatedUsers(data.recipient);
            const grantorIndex = grantorAddress.data.findIndex(item => data.grantor.toLowerCase() === item.toLowerCase());
            if (grantorIndex === -1) {
                throw ("Patient mapping not available against the doctor");
            }

            let patientIndex = parseInt(data.provider) - 1;

            const params = {
                recipient : data.recipient,
                grantorIndex,
                patientEmrIndex: patientIndex
            };
            const grantorTx = await transaction.getFile(data.user, params);
            return grantorTx;
        }catch(err){
            throw err;
        }
    },

    getFile: async (data) => {
        try {
            if (data) {
                if (data.user.toLowerCase() === "patient" && data.ssn && data.account) {
                    const user = await DBService.getUserDetails({ Adhaar: data.ssn });
                    console.log("user", user[0])
                    const userCredentials = decrypt(user[0].cipher);
                    console.log("userCredentials", userCredentials)
                    if (!userCredentials || !userCredentials.userName || !userCredentials.password || !userCredentials.phoneNumber) throw new Error({ message: "Invalid user credentials." })
                    const blockIdentifier = getUuidByString(userCredentials.userName + userCredentials.password + userCredentials.phoneNumber, config.APP).replace(/-/g, '');
                    console.log("blockIdentifier", blockIdentifier)
                    const params =  {
                        account : data.account,
                        blockId: blockIdentifier
                    }
                    const getFileTx = await transaction.getFile(data.user, params);
                    console.log(getFileTx);
                    return getFileTx;
                }
                else if (data.user.toLowerCase() === "doctor" && data.grantor && data.recipient) {
                    const grantorAddress = await transaction.getAllocatedUsers(data.recipient);
                    console.log("userAddress", grantorAddress.data);
                    const grantorIndex = grantorAddress.data.findIndex(item => data.grantor.toLowerCase() === item.toLowerCase());
                    if (grantorIndex !== -1) {
                        const params = {
                            recipient : data.recipient,
                            grantorIndex
                        }
                        const grantorTx = await transaction.getFile(data.user, params);
                        console.log(grantorTx);
                        return grantorTx;
                    }
                    else {
                        throw ("Patient mapping not available against the doctor");
                    }
                }
                else {
                    throw ("Invalid request. Insufficient parameters.")
                }
            }
            else {
                throw "Request malformed. Please provide user address and ssn"
            }

        }
        catch (e) { throw e; }
    },

    getProviders: async (data) => {
        try {
            let userProviderList;
            if(data.address){
                userProviderList = await helper.providerListByAccount(data);

            }else if (data.userId){
                userProviderList = await helper.providerListBySsn(data);
            }


            return userProviderList;

        }
        catch (e) { throw e; }
    },

    checkUserExist : async (data) => {

        try {

            let providerId = data.providerId;
            let emrId = data.emrId;
            return await openmrs.isUserPresent(providerId,emrId);

        }catch(e){

            { throw e; }

        }
    },

    updateUserProfile : async (patientId, data) => {

        try {

            let patientResourceBody = data.patientResourceData;
            let providerId = data.userCredentials.emrDetails.number;

            return await openmrs.updateUserProfile(patientResourceBody,providerId,patientId);

        } catch (e) {
            throw e;
        }


    }

}
