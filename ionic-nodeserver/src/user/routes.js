import express from "express";
import PATH from "path";
import controller from "./controller";
const router = express.Router();


router.get('/grantor', async (req, res, next) => {
    try {
        const data = req.query;
        const user = await controller.getGrantors(data);
        res.status(200).send(user);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.post('/generateotp', async (req, res, next) => {
    try {
        const data = req.body;
        console.log("data", data);
        const userUuid = await controller.generateotp(data);
        res.status(200).send({message: "OTP generated successfully", userId: userUuid});
    }
    catch (e) {
        res.status(400).send({message: e.message});
    }
});

router.post('/register', async (req, res, next) => {
    try {
        const data = req.body;
        const globalId = await controller.register(data);
        res.status(200).send({message: "Registration successful", globalId: globalId});
    }
    catch (e) {
        res.status(400).send({message: e.message});
    }
});

router.post ('/login', async (req, res, next) => {
    try {
        const data = req.body;
        const result = await controller.login(data);
        res.status(200).send(result);
    }
    catch (e) {
        res.status(500).send(e);
    }
});


router.post('/upload', async (req, res, next) => {
    try {
        const data = req.body;
        console.log(data);
        const response = await controller.attachFiletoUser(data);
        res.status(200).send(response);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/visits', async (req, res, next) => {
    try {
        const data = req.query;
        console.log(data);
        const response = await controller.getVisits(data);
        res.status(200).send(response);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/observations', async (req, res, next) => {
    try {
        const data = req.query;
        const response = await controller.getObservations(data);
        res.status(200).send(response);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:userId/imagingstudy', async (req, res, next) => {

    try {
        const response = await controller.getImagingStudyForPatient(req.params.userId, req.query.provider);
        let result = JSON.parse(response);
        res.status(200).send(result);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/allocated', async (req, res, next) => {
    try {
        const data = req.query;
        console.log(data);
        const users = await controller.getAllocatedUsers(data);
        console.log(users);
        res.status(200).send(users);
    }
    catch (e) { res.status(500).send(e); }
});

router.post('/file', async (req, res, next) => {
    try {
        const data = req.body;
        let fileUrl;
        if(data.user.toLowerCase()==='patient') {
            fileUrl = await controller.getFileFromPatient(data);
        } else if (data.user.toLowerCase() === 'doctor') {
           fileUrl = await controller.getPatientFileFromDoctor(data);
        }
        res.status(200).send(fileUrl);
    }
    catch (e) {
        res.status(500).send(e);
    }
});

router.get('/providerList', async (req, res, next) => {
    try {
        console.log("calling providerlist on server part");
        const data = req.query;
        console.log(data);
        const providerList = await controller.getProviders(data);
        console.log(providerList);
        res.status(200).send(providerList);
    }
    catch (e) { res.status(500).send(e); }
})

//search api to see if user is already existing in the EMR system

router.get('/', async (req, res, next) => {

    try {

        const data = req.query;
        const result = await controller.checkUserExist(data);

        res.status(200).send(JSON.parse(result));

    }
    catch (e) { res.status(500).send(e); }
})

router.put('/:userId', async (req, res, next) => {

    const patientId = req.params.userId;
    const data = req.body;

    try {

        const result = await controller.updateUserProfile(patientId, data);
        res.status(200).send({message:result});

    }
    catch (e) { res.status(500).send(e); }
})



module.exports = router;
