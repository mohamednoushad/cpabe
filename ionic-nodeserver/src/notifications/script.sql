CREATE TABLE `Notifications` (
  `Id` VARCHAR(45) NOT NULL,
  `Message` VARCHAR(500) NULL,
  `UserId` VARCHAR(100) NULL,
  `IsRead` INT(1) NULL DEFAULT 0,
  PRIMARY KEY (`Id`));

ALTER TABLE `Notifications`
ADD COLUMN `CreatedOn` DATETIME NULL AFTER `IsRead`;
