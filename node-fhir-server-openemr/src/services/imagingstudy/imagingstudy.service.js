/*eslint no-unused-vars: "warn"*/

const { RESOURCES } = require('@asymmetrik/node-fhir-server-core').constants;
const FHIRServer = require('@asymmetrik/node-fhir-server-core');
const globals = require('../../globals');
const { CLIENT, BASE_URL , PACS_BASE_URL} = require('../../constants');

let getImagingStudy = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.IMAGINGSTUDY));};

let getMeta = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.META));};

function mapImagingStudyResultToImagingStudyFHIRResource(study){
    let base_version = '3_0_1';

    let ImagingStudy = getImagingStudy(base_version);
    let fhirResource = new ImagingStudy();
    fhirResource.id = study.id;
    fhirResource.uid = study.study_instance_uid;
    fhirResource.identifier = [
        {use: 'usual', system: 'https://www.open-emr.org', value: study.id },
    ];

    fhirResource.patient = {
        'id': study.patientId,
        'reference': `Patient/${study.patientId}`
    };


    fhirResource. context = {
        'id': study.encounterId,
        'reference': `Encounter/${study.encounterId}`
    };

    fhirResource.availability = 'available';

    fhirResource.description = study.study_description;

    fhirResource.referrer =
        {
            'reference': `Practitioner/${study.referrerId}`,
            'display': `${study.referrerFirstName} ${study.referrerLastName}`
        };


    fhirResource.interpreter = [
        {
            'reference': `Practitioner/${study.interpreterId}`,
            'display': `${study.interpreterFirstName} ${study.interpreterLastName}`
        }
    ];


    fhirResource.started = study.study_date;

    fhirResource.procedureCode = [{
        'coding': [
            {
                'system': 'http://snomed.info/sct',
                'code': study.procedure_code,
                display: study.procedure_name
            }
        ],
    }];

    fhirResource.reason = {
        'coding': [
            {
                'system': 'http://snomed.info/sct',
                'code': study.reasonCode,
                display: study.reason
            }
        ],
    };

    fhirResource.description = study.study_description;
    fhirResource.numberOfSeries = 0;
    let studySeries = [];
    for (let seriesData of study.series){
        let series = {
            uid: seriesData.series_instance_id,
            modality:
                {
                    'system': 'http://dicom.nema.org/resources/ontology/DCM',
                    'code': seriesData.series_modaliity
                }
            ,
            bodySite: {
                'system': 'http://snomed.info/sct',
                'code': study.bodySiteCode,
                'display': study.bodySite
            },
            description: seriesData.series_description,
            started: seriesData.series_date
        };

        series.endpoint = [];
        series.instance = [];
        series.numberOfInstances = 0;
        for (let instance of seriesData.instances){
            let instanceData = {
                uid: instance.instance_uid,
                number: instance.instance_number,
                sopClass: instance.sop_class_uid
            };
            series.endpoint.push({ reference: `${PACS_BASE_URL}/dcm4chee-arc/aets/DCM4CHEE/wado?requestType=WADO&studyUID=${study.study_instance_uid}&seriesUID=${seriesData.series_instance_id}&objectUID=${instance.instance_uid}&contentType=application/dicom&transferSyntax=*`})
            series.instance.push(instanceData);
            series.numberOfInstances++;
        }
        fhirResource.numberOfSeries++;
        studySeries.push(series);
    }


    fhirResource.series = studySeries;
    fhirResource.meta = {versionId: 1, lastUpdated: study.study_date};

    return fhirResource;


}

//group imaging study instances into study and series
function groupImagingStudyInstances(imagingDiagnosticResults){
    let response = [];
    //group instances by study id as key
    if (imagingDiagnosticResults && imagingDiagnosticResults.length !== 0) {
        let groupedStudies = imagingDiagnosticResults.reduce(function (r, a) {
            r[a.id] = r[a.id] || [];
            r[a.id].push(a);
            return r;
        }, Object.create(null));

        for (let key in groupedStudies) {
            let studyInstances = groupedStudies[key];
            let studyDetails;
            if (studyInstances && studyInstances.length !== 0) {
                studyDetails = {
                    id: studyInstances[0].id,
                    study_instance_uid: studyInstances[0].study_instance_uid,
                    study_date: studyInstances[0].study_date,
                    study_time: studyInstances[0].study_time,
                    study_description: studyInstances[0].study_description,
                    patientId: studyInstances[0].patientId,
                    accession_number: studyInstances[0].accession_number,
                    bodySiteCode: studyInstances[0].bodySiteCode,
                    bodySite: studyInstances[0].bodySite,
                    encounterId: studyInstances[0].encounterId,
                    conclusion: studyInstances[0].conclusion,
                    procedure_code: studyInstances[0].procedure_code,
                    procedure_name: studyInstances[0].procedure_name,
                    reasonCode: studyInstances[0].reasonCode,
                    reason: studyInstances[0].reason,
                    referrerId: studyInstances[0].referrerId,
                    referrerFirstName: studyInstances[0].referrerFirstName,
                    referrerLastName: studyInstances[0].referrerLastName,
                    interpreterId: studyInstances[0].interpreterId,
                    interpreterFirstName: studyInstances[0].interpreterFirstName,
                    interpreterLastName: studyInstances[0].interpreterLastName
                };
                studyDetails.series = [];
                studyDetails.conclusionCodes = [];
                //group instances within a study into resries by series id key
                let groupedSeries = studyInstances.reduce(function (r, a) {
                    r[a.series_instance_id] = r[a.series_instance_id] || [];
                    r[a.series_instance_id].push(a);
                    return r;
                }, Object.create(null));
                for (let skey in groupedSeries) {
                    let seriesInstances = groupedSeries[skey];
                    if (seriesInstances && seriesInstances.length !== 0) {
                        let seriesDetails = {
                            series_instance_id: seriesInstances[0].series_instance_id,
                            series_modaliity: seriesInstances[0].series_modaliity,
                            series_description: seriesInstances[0].series_description,
                            series_date: seriesInstances[0].series_date,
                            series_time: seriesInstances[0].series_time,
                            series_body_site: seriesInstances[0].series_body_site
                        };

                        let instances = [];
                        for (let instance of seriesInstances){
                            let hasInstance = instances.find(y => y.instance_uid === instance.instance_uid);
                            if (!hasInstance) {
                                let instance_details = {
                                    instance_uid: instance.instance_uid,
                                    instance_number: instance.instance_number,
                                    sop_class_uid: instance.sop_class_uid
                                };

                                instances.push(instance_details);
                            }

                            let hasConcCode = studyDetails.conclusionCodes.find(p => p.conclusion_code_id === instance.conclusion_code_id);
                            if (!hasConcCode){
                                let conclusionCode = {
                                    conclusion_code_id: instance.conclusion_code_id,
                                    conclusion_code: instance.conclusion_code,
                                    conclusion_code_term: instance.conclusion_code_term
                                };
                                studyDetails.conclusionCodes.push(conclusionCode);
                            }
                        }

                        seriesDetails.instances = instances;
                        let hasSeries = studyDetails.series.find(z => z.series_instance_id === seriesDetails.series_instance_id);
                        if (!hasSeries){
                            studyDetails.series.push(seriesDetails);
                        }

                    }

                }

            }
            let imagingStudyResult = mapImagingStudyResultToImagingStudyFHIRResource(studyDetails);
            response.push(imagingStudyResult);
        }
    }

    return response;
}


let buildStu3SearchQuery = (args) => {
    // Common search params
    let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

    // Search Result params
    let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

    // Resource Specific params
    let accession = args['accession'];
    let basedon = args['basedon'];
    let bodysite = args['bodysite'];
    let _context = args['_context'];
    let dicom_class = args['dicom-class'];
    let endpoint = args['endpoint'];
    let identifier = args['identifier'];
    let modality = args['modality'];
    let patient = args['patient'];
    let performer = args['performer'];
    let reason = args['reason'];
    let series = args['series'];
    let started = args['started'];
    let study = args['study'];
    let uid = args['uid'];

    let query = `SELECT imaging_study.id, study_instance_uid, study_date, study_time, study_description, series_instance_id, series_modaliity, series_description, series_date, series_time, instance_uid, instance_number,sop_class_uid, series_body_site,
imaging_study.patientId, imaging_study.accession_number, imaging_study.bodySiteCode, imaging_study.bodySite, imaging_study.encounterId, imaging_study.conclusion,
imaging_study_conclusion_codes.codeId AS conclusion_code_id, imaging_study_conclusion_codes.conclusion_code, imaging_study_conclusion_codes.conclusion_code_term,
imaging_order_code.procedure_code, imaging_order_code.procedure_name, imaging_order_code.diagnoses AS reasonCode, imaging_order_code.reason,
referrer.id AS referrerId, referrer.fname AS referrerFirstName,referrer.lname AS referrerLastName,
interpreter.id AS interpreterId, interpreter.fname AS interpreterFirstName,interpreter.lname AS interpreterLastName
FROM openemr.imaging_study_instances JOIN imaging_study ON imaging_study_instances.study_instance_uid = imaging_study.studyId
JOIN imaging_order ON imaging_study.imaging_order_id = imaging_order.imaging_order_id
LEFT JOIN imaging_order_code ON imaging_order_code.imaging_order_id = imaging_order.imaging_order_id
LEFT JOIN imaging_study_conclusion_codes ON imaging_study_conclusion_codes.study_id = imaging_study.id
LEFT JOIN users AS referrer ON imaging_order.provider_id = referrer.id
LEFT JOIN users AS interpreter ON imaging_order.provider_id = interpreter.id`;
    let params = [];

    let patientId;
    if (patient){
        let data = patient.split('/');
        patientId = data[1];
    }

    let whereFilter = ' WHERE';
    console.log("see patient id",patientId);
    if (patientId){
        whereFilter += ` imaging_study.review_status = 1 AND imaging_study.patientId = ? `;
        params.push(patientId);
    }

    let lastUpdated = args['_lastUpdated'];
    if (lastUpdated){
        if (params.length ) {
            whereFilter += ' AND';
        }
        let order = lastUpdated.substring(0, 2);
        if (order === 'lt'){
            whereFilter += ' imaging_order.date_ordered < ? ';
        }
        else {
            whereFilter += ' imaging_order.date_ordered > ? ';
        }

        let lastUpdatedDate = lastUpdated.substr(2);
        params.push(lastUpdatedDate);
    }

    if (params.length) {
        query += whereFilter;
    }

    let sort = args['_sort'];
    if (sort && sort === '_lastUpdated'){
        query += ' ORDER BY imaging_order.date_ordered';
    }

    return {query, params};
};

module.exports.search = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('ImagingStudy >>> search');

    let base_version = '3_0_1';

	let ImagingStudy = getImagingStudy(base_version);

    let connection = globals.get(CLIENT);
    let {query, params} = buildStu3SearchQuery(args);
    connection.query(query, params, function (err, imagingDiagnosticResults, fields){
        if (err) {
            logger.error('Error with ImagingSTudy.search: ', err);
            reject(err);
        }
        else {
            console.log(imagingDiagnosticResults);
            let response = groupImagingStudyInstances(imagingDiagnosticResults);
            resolve(response);
        }
        resolve();
    });
});

function getImagingStudyWithIdQuery(studyId){
    let query = `SELECT imaging_study.id, study_instance_uid, study_date, study_time, study_description, series_instance_id, series_modaliity, series_description, series_date, series_time, instance_uid, instance_number,sop_class_uid, series_body_site,
imaging_study.patientId, imaging_study.accession_number, imaging_study.bodySiteCode, imaging_study.bodySite, imaging_study.encounterId, imaging_study.conclusion,
imaging_study_conclusion_codes.codeId AS conclusion_code_id, imaging_study_conclusion_codes.conclusion_code, imaging_study_conclusion_codes.conclusion_code_term,
imaging_order_code.procedure_code, imaging_order_code.procedure_name, imaging_order_code.diagnoses AS reasonCode, imaging_order_code.reason,
referrer.id AS referrerId, referrer.fname AS referrerFirstName,referrer.lname AS referrerLastName,
interpreter.id AS interpreterId, interpreter.fname AS interpreterFirstName,interpreter.lname AS interpreterLastName
FROM openemr.imaging_study_instances JOIN imaging_study ON imaging_study_instances.study_instance_uid = imaging_study.studyId
JOIN imaging_order ON imaging_study.imaging_order_id = imaging_order.imaging_order_id
LEFT JOIN imaging_order_code ON imaging_order_code.imaging_order_id = imaging_order.imaging_order_id
LEFT JOIN imaging_study_conclusion_codes ON imaging_study_conclusion_codes.study_id = imaging_study.id
LEFT JOIN users AS referrer ON imaging_order.provider_id = referrer.id
LEFT JOIN users AS interpreter ON imaging_order.provider_id = interpreter.id
     WHERE imaging_study.id = ?;`;

    return {query, params: [studyId]};
}

module.exports.searchById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('ImagingStudy >>> searchById');

	let { base_version, id } = args;

	let ImagingStudy = getImagingStudy(base_version);

    let connection = globals.get(CLIENT);
    let {query, params} = getImagingStudyWithIdQuery(id);
    connection.query(query, params, function (err, imagingDiagnosticResults, fields){
        if (err) {
            logger.error('Error with ImagingSTudy.searchById: ', err);
            reject(err);
        }
        else {
               let response = groupImagingStudyInstances(imagingDiagnosticResults);
              resolve(response[0]);
        }
        resolve();
    });
});



module.exports.create = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('ImagingStudy >>> create');

	let { base_version, id, resource } = args;

	let ImagingStudy = getImagingStudy(base_version);
	let Meta = getMeta(base_version);

	// TODO: determine if client/server sets ID

	// Cast resource to ImagingStudy Class
	let imagingstudy_resource = new ImagingStudy(resource);
	imagingstudy_resource.meta = new Meta();
	// TODO: set meta info

	// TODO: save record to database

	// Return Id
	resolve({ id: imagingstudy_resource.id });
});

module.exports.update = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('ImagingStudy >>> update');

	let { base_version, id, resource } = args;

	let ImagingStudy = getImagingStudy(base_version);
	let Meta = getMeta(base_version);

	// Cast resource to ImagingStudy Class
	let imagingstudy_resource = new ImagingStudy(resource);
	imagingstudy_resource.meta = new Meta();
	// TODO: set meta info, increment meta ID

	// TODO: save record to database

	// Return id, if recorded was created or updated, new meta version id
	resolve({ id: imagingstudy_resource.id, created: false, resource_version: imagingstudy_resource.meta.versionId });
});

module.exports.remove = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('ImagingStudy >>> remove');

	let { id } = args;

	// TODO: delete record in database (soft/hard)

	// Return number of records deleted
	resolve({ deleted: 0 });
});

module.exports.searchByVersionId = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('ImagingStudy >>> searchByVersionId');

	let { base_version, id, version_id } = args;

	let ImagingStudy = getImagingStudy(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to ImagingStudy Class
	let imagingstudy_resource = new ImagingStudy();

	// Return resource class
	resolve(imagingstudy_resource);
});

module.exports.history = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('ImagingStudy >>> history');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let accession = args['accession'];
	let basedon = args['basedon'];
	let bodysite = args['bodysite'];
	let _context = args['_context'];
	let dicom_class = args['dicom-class'];
	let endpoint = args['endpoint'];
	let identifier = args['identifier'];
	let modality = args['modality'];
	let patient = args['patient'];
	let performer = args['performer'];
	let reason = args['reason'];
	let series = args['series'];
	let started = args['started'];
	let study = args['study'];
	let uid = args['uid'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let ImagingStudy = getImagingStudy(base_version);

	// Cast all results to ImagingStudy Class
	let imagingstudy_resource = new ImagingStudy();

	// Return Array
	resolve([imagingstudy_resource]);
});

module.exports.historyById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('ImagingStudy >>> historyById');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let accession = args['accession'];
	let basedon = args['basedon'];
	let bodysite = args['bodysite'];
	let _context = args['_context'];
	let dicom_class = args['dicom-class'];
	let endpoint = args['endpoint'];
	let identifier = args['identifier'];
	let modality = args['modality'];
	let patient = args['patient'];
	let performer = args['performer'];
	let reason = args['reason'];
	let series = args['series'];
	let started = args['started'];
	let study = args['study'];
	let uid = args['uid'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let ImagingStudy = getImagingStudy(base_version);

	// Cast all results to ImagingStudy Class
	let imagingstudy_resource = new ImagingStudy();

	// Return Array
	resolve([imagingstudy_resource]);
});

//for patient everything api
module.exports.patientImagingStudy = (args, context, logger) => new Promise((resolve, reject) => {
    logger.info('ImagingStudy >>> patientImagingStudy');

    let base_version = '3_0_1';

    let ImagingStudy = getImagingStudy(base_version);

    let connection = globals.get(CLIENT);
    let {query, params} = buildStu3SearchQuery(args);
    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with ImagingSTudy.patientImagingStudy: ', err);
            reject(err);
        }
        else {
            let response = [];

            if (results && results.length !== 0) {
                let imagingStudyResults = groupImagingStudyInstances(results);
                for (let data of imagingStudyResults) {
                    response.push({fullUrl: `${BASE_URL}/ImagingStudy/${data.id}`, resource: data});
                }
                resolve(response);
            }
        }
        resolve();
    });
});
