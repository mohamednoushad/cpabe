/*eslint no-unused-vars: "warn"*/

const { RESOURCES } = require('@asymmetrik/node-fhir-server-core').constants;
const FHIRServer = require('@asymmetrik/node-fhir-server-core');
const globals = require('../../globals');
const { CLIENT } = require('../../constants');

let getAllergyIntolerance = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.ALLERGYINTOLERANCE));};

let getMeta = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.META));};

function buildStu3SearchQuery(args) {

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let abatement_age = args['abatement-age'];
	let abatement_boolean = args['abatement-boolean'];
	let abatement_date = args['abatement-date'];
	let abatement_string = args['abatement-string'];
	let asserted_date = args['asserted-date'];
	let asserter = args['asserter'];
	let body_site = args['body-site'];
	let category = args['category'];
	let clinical_status = args['clinical-status'];
	let code = args['code'];
	let _context = args['_context'];
	let encounter = args['encounter'];
	let evidence = args['evidence'];
	let evidence_detail = args['evidence-detail'];
	let identifier = args['identifier'];
	let onset_age = args['onset-age'];
	let onset_date = args['onset-date'];
	let onset_info = args['onset-info'];
	let patient = args['patient'];
	let severity = args['severity'];
	let stage = args['stage'];
	let subject = args['subject'];
	let verification_status = args['verification-status'];

	let query = `SELECT lst.id AS allergy_id,
				lst.date,
                lst.title AS allergy,
                lst.begdate,
                lst.diagnosis AS allergy_code,
                lst.severity_al AS severity,
                lst.severity_code,
                lst.clinical_status,
                lst.symptom_code,
                lst.symptom_name,
                lst.verification_status,
                lst.comments,
                lst.category,
                lst.substance_code,
             	lst.substance_name,
             	lst.last_occured_date,
                lst.pid,
				asserter.id AS asserterId, asserter.fname AS asserterFirstName,asserter.lname AS asserterLastName,
                encounter.encounter AS encounter
FROM lists AS lst
LEFT JOIN issue_encounter AS encounter ON encounter.list_id = lst.id
LEFT JOIN users AS asserter ON lst.user = asserter.id
 WHERE lst.type = ? `;

	let params = ['allergy'];

	let whereFilter = ' ';

	if (subject){
		let data = subject.split('/');
		let patientId = data[1];
		if (params.length) {
			whereFilter += ' AND';
		}
		whereFilter += ' lst.pid = ? ';
		params.push(+patientId);
	}

	let lastUpdated = args['_lastUpdated'];
	if (lastUpdated){
		if (params.length ) {
			whereFilter += ' AND';
		}
		let order = lastUpdated.substring(0, 2);
		if (order === 'lt'){
			whereFilter += ' lst.date < ? ';
		}
		else {
			whereFilter += ' lst.date > ? ';
		}

		let lastUpdatedDate = lastUpdated.substr(2);
		params.push(lastUpdatedDate);
	}

	if (params.length) {
		query += whereFilter;
	}

	let sort = args['_sort'];
	if (sort && sort === '_lastUpdated'){
		query += ' ORDER BY lst.date';
	}

	return {query, params};
}

function mapAllergyResultToAllergyIntoleranceFHIRResource(allergy) {
	let fhirResource = {id: allergy.allergy_id};
	fhirResource.identifier = [
		{use: 'usual', system: 'https://www.open-emr.org', value: allergy.allergy_id },
	];
	fhirResource.clinicalStatus = allergy.clinical_status;
	fhirResource.verificationStatus = allergy.verification_status;

	fhirResource.category = [
		allergy.category
	];

	fhirResource.code = {
		'coding': [
			{
				'system': 'http://snomed.info/sct',
				'code': allergy.allergy_code,
				'display': allergy.allergy
			}
		]
	};

	fhirResource.context = {
		'id': allergy.encounter,
		'reference': `Encounter/${allergy.encounter}`
	};

	fhirResource.subject = {
		'id': allergy.pid,
		'reference': `Patient/${allergy.pid}`
	};

	fhirResource.onsetDateTime = allergy.begdate;
	fhirResource.lastOccurence = allergy.last_occured_date;

	fhirResource.asserter = {
		'reference': `Practitioner/${allergy.asserterId}`,
		'display': `${allergy.asserterFirstName} ${allergy.asserterLastName}`
	};


	fhirResource.reaction = [
		{
			'substance': {
				'coding': [
					{
						'system': 'http://snomed.info/sct',
						'code': allergy.substance_code,
						'display': allergy.substance_name
					}
				]
			},
			'manifestation': [
				{
					'coding': [
						{
							'system': 'http://snomed.info/sct',
							'code': allergy.symptom_code,
							'display': allergy.symptom_name
						}
					]
				}
			],
			'severity': allergy.severity
		}];

	fhirResource.note = [{text: allergy.comments}];

	fhirResource.meta = {versionId: 1, lastUpdated: allergy.date};

	return fhirResource;
}

module.exports.search = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('AllergyIntolerance >>> search');

	let base_version = '3_0_1';
	let {query, params} = buildStu3SearchQuery(args);

	let AllergyIntolerance = getAllergyIntolerance(base_version);

	let connection = globals.get(CLIENT);

	connection.query(query, params, function (err, results, fields){
		if (err) {
			logger.error('Error with AllergyIntolerance.search: ', err);
			reject(err);
		}
		else {
			let response = [];
			if (results && results.length !== 0) {
				for (let data of results) {
					let allergyResult = mapAllergyResultToAllergyIntoleranceFHIRResource(data);
					response.push(new AllergyIntolerance(allergyResult));
				}
			}
			resolve(response);
		}
	});
});

module.exports.searchById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('AllergyIntolerance >>> searchById');

	let { base_version, id } = args;

	let AllergyIntolerance = getAllergyIntolerance(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to AllergyIntolerance Class
	let allergyintolerance_resource = new AllergyIntolerance();
	// TODO: Set data with constructor or setter methods
	allergyintolerance_resource.id = 'test id';

	// Return resource class
	// resolve(allergyintolerance_resource);
	resolve();
});

module.exports.create = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('AllergyIntolerance >>> create');

	let { base_version, id, resource } = args;

	let AllergyIntolerance = getAllergyIntolerance(base_version);
	let Meta = getMeta(base_version);

	// TODO: determine if client/server sets ID

	// Cast resource to AllergyIntolerance Class
	let allergyintolerance_resource = new AllergyIntolerance(resource);
	allergyintolerance_resource.meta = new Meta();
	// TODO: set meta info

	// TODO: save record to database

	// Return Id
	resolve({ id: allergyintolerance_resource.id });
});

module.exports.update = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('AllergyIntolerance >>> update');

	let { base_version, id, resource } = args;

	let AllergyIntolerance = getAllergyIntolerance(base_version);
	let Meta = getMeta(base_version);

	// Cast resource to AllergyIntolerance Class
	let allergyintolerance_resource = new AllergyIntolerance(resource);
	allergyintolerance_resource.meta = new Meta();
	// TODO: set meta info, increment meta ID

	// TODO: save record to database

	// Return id, if recorded was created or updated, new meta version id
	resolve({ id: allergyintolerance_resource.id, created: false, resource_version: allergyintolerance_resource.meta.versionId });
});

module.exports.remove = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('AllergyIntolerance >>> remove');

	let { id } = args;

	// TODO: delete record in database (soft/hard)

	// Return number of records deleted
	resolve({ deleted: 0 });
});

module.exports.searchByVersionId = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('AllergyIntolerance >>> searchByVersionId');

	let { base_version, id, version_id } = args;

	let AllergyIntolerance = getAllergyIntolerance(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to AllergyIntolerance Class
	let allergyintolerance_resource = new AllergyIntolerance();

	// Return resource class
	resolve(allergyintolerance_resource);
});

module.exports.history = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('AllergyIntolerance >>> history');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let asserter = args['asserter'];
	let category = args['category'];
	let clinical_status = args['clinical-status'];
	let code = args['code'];
	let criticality = args['criticality'];
	let date = args['date'];
	let identifier = args['identifier'];
	let last_date = args['last-date'];
	let manifestation = args['manifestation'];
	let onset = args['onset'];
	let patient = args['patient'];
	let recorder = args['recorder'];
	let route = args['route'];
	let severity = args['severity'];
	let type = args['type'];
	let verification_status = args['verification-status'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let AllergyIntolerance = getAllergyIntolerance(base_version);

	// Cast all results to AllergyIntolerance Class
	let allergyintolerance_resource = new AllergyIntolerance();

	// Return Array
	resolve([allergyintolerance_resource]);
});

module.exports.historyById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('AllergyIntolerance >>> historyById');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let asserter = args['asserter'];
	let category = args['category'];
	let clinical_status = args['clinical-status'];
	let code = args['code'];
	let criticality = args['criticality'];
	let date = args['date'];
	let identifier = args['identifier'];
	let last_date = args['last-date'];
	let manifestation = args['manifestation'];
	let onset = args['onset'];
	let patient = args['patient'];
	let recorder = args['recorder'];
	let route = args['route'];
	let severity = args['severity'];
	let type = args['type'];
	let verification_status = args['verification-status'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let AllergyIntolerance = getAllergyIntolerance(base_version);

	// Cast all results to AllergyIntolerance Class
	let allergyintolerance_resource = new AllergyIntolerance();

	// Return Array
	resolve([allergyintolerance_resource]);
});

