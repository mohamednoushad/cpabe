UPDATE `openemr`.`globals` SET `gl_value`='DWellize' WHERE `gl_name`='openemr_name' and`gl_index`='0';

INSERT INTO `openemr`.`codes` (`id`, `code_text`, `code_text_short`, `code`, `code_type`, `modifier`, `units`, `fee`, `superbill`, `related_code`, `taxrates`, `cyp_factor`, `active`, `reportable`, `financial_reporting`, `revenue_code`) VALUES ('160', 'Systolic blood pressure', 'BP sys', '8480-6', '110', '', '0', '0.00', '', '', '', '0', '1', '0', '0', '');
INSERT INTO `openemr`.`codes` (`id`, `code_text`, `code_text_short`, `code`, `code_type`, `modifier`, `units`, `fee`, `superbill`, `related_code`, `taxrates`, `cyp_factor`, `active`, `reportable`, `financial_reporting`, `revenue_code`) VALUES ('161', 'Diastolic blood pressure', 'BP dias', '8462-4', '110', '', '0', '0.00', '', '', '', '0', '1', '0', '0', '');

INSERT INTO `openemr`.`codes` (`id`, `code_text`, `code_text_short`, `code`, `code_type`, `active`) VALUES ('162', 'Malignant tumor of craniopharyngeal duct (disorder)', 'Malignant tumor', '188340000', '107', '1');


INSERT INTO `openemr`.`codes` (`id`, `code_text`, `code_text_short`, `code`, `code_type`, `active`) VALUES ('163', 'Head and neck', 'Head and neck', '774007', '107', '1');



