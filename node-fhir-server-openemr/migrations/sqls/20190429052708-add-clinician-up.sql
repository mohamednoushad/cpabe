INSERT INTO `openemr`.`users` (`id`, `username`, `password`, `authorized`, `fname`, `lname`, `facility`, `facility_id`, `see_auth`, `active`, `email_direct`, `cal_ui`, `taxonomy`, `calendar`, `abook_type`, `default_warehouse`, `irnpool`, `main_menu_role`, `patient_menu_role`) VALUES ('7', 'clinician', 'NoLongerUsed', '1', 'Tom', 'Cruise', 'Abc Clinic', '3', '1', '1', '', '3', '207Q00000X', '1', '', '', '', 'clinician', 'standard');

INSERT INTO `openemr`.`users_secure` (`id`, `username`, `password`, `salt`, `last_update`) VALUES ('7', 'clinician', '$2a$05$DGmvibdJletCJf5Fe8CzY.QSV.tNdUWIkrvxttZFFuvUGyi8vtbwS', '$2a$05$DGmvibdJletCJf5Fe8CzY/$', '2018-12-10 18:30:45');

INSERT INTO `openemr`.`gacl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES ('13', 'users', 'clinician', '10', 'Clinician', '0');

INSERT INTO `openemr`.`gacl_groups_aro_map` (`group_id`, `aro_id`) VALUES ('12', '13');

INSERT INTO `openemr`.`groups` (`id`, `name`, `user`) VALUES ('7', 'Default', 'clinician');

