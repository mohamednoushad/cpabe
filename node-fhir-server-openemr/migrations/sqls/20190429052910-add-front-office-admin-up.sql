INSERT INTO `openemr`.`gacl_aro_groups` (`id`, `parent_id`, `lft`, `rgt`, `name`, `value`) VALUES ('18', '12', '4', '5', 'Nurse', 'nurse');

INSERT INTO `openemr`.`users` (`id`, `username`, `password`, `authorized`, `fname`, `lname`, `facility`, `facility_id`, `see_auth`, `active`, `email_direct`, `cal_ui`, `taxonomy`, `calendar`, `abook_type`, `default_warehouse`, `irnpool`, `main_menu_role`, `patient_menu_role`) VALUES ('9', 'nurse', 'NoLongerUsed', '1', 'Claire', 'Denis', 'Abc Clinic', '3', '1', '1', '', '3', '207Q00000X', '1', '', '', '', 'clinician', 'standard');

INSERT INTO `openemr`.`users_secure` (`id`, `username`, `password`, `salt`, `last_update`) VALUES ('9', 'nurse', '$2a$05$DGmvibdJletCJf5Fe8CzY.QSV.tNdUWIkrvxttZFFuvUGyi8vtbwS', '$2a$05$DGmvibdJletCJf5Fe8CzY/$', '2018-12-10 18:30:45');

INSERT INTO `openemr`.`gacl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES ('15', 'users', 'nurse', '10', 'Nurse', '0');

INSERT INTO `openemr`.`gacl_groups_aro_map` (`group_id`, `aro_id`) VALUES ('18', '15');

INSERT INTO `openemr`.`groups` (`id`, `name`, `user`) VALUES ('9', 'Default', 'nurse');

INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('36', 'system', '1', '1', 'view', 'Things that nurse can only read', '1544446848');
INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('37', 'system', '1', '1', 'addonly', 'Things that nurse can read and enter but not modify', '1544446848');
INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('38', 'system', '1', '1', 'wsome', 'Things that nurse can read and partly modify', '1544446848');
INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('39', 'system', '1', '1', 'write', 'Things that nurse can read and modify', '1544446848');

INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('36', '18');
INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('37', '18');
INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('38', '18');
INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('39', '18');

