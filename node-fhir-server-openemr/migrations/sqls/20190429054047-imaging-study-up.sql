DROP TABLE IF EXISTS `imaging_study`;

CREATE TABLE `imaging_study` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studyId` varchar(150) DEFAULT NULL,
  `modality` varchar(45) DEFAULT NULL,
  `patientId` int(11) DEFAULT NULL,
  `conclusion` varchar(1000) DEFAULT NULL,
  `bodySite` varchar(250) DEFAULT NULL,
  `encounterId` int(11) DEFAULT NULL,
  `imaging_order_id` int(11) DEFAULT NULL,
  `accession_number` varchar(255) DEFAULT NULL,
  `bodySiteCode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

ALTER TABLE `openemr`.`imaging_study`
ADD COLUMN `reviewer` INT NULL AFTER `bodySiteCode`,
ADD COLUMN `review_status` VARCHAR(45) NULL AFTER `reviewer`;


CREATE TABLE `imaging_study_conclusion_codes` (
  `codeId` int(11) NOT NULL AUTO_INCREMENT,
  `conclusion_code` varchar(45) DEFAULT NULL,
  `conclusion_code_term` varchar(250) DEFAULT NULL,
  `study_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`codeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `imaging_study_instances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `study_instance_uid` varchar(150) DEFAULT NULL,
  `study_date` date DEFAULT NULL,
  `study_time` time DEFAULT NULL,
  `study_description` varchar(500) DEFAULT NULL,
  `series_instance_id` varchar(150) DEFAULT NULL,
  `series_modaliity` varchar(10) DEFAULT NULL,
  `series_description` varchar(500) DEFAULT NULL,
  `series_date` date DEFAULT NULL,
  `series_time` time DEFAULT NULL,
  `instance_uid` varchar(150) DEFAULT NULL,
  `instance_number` int(11) DEFAULT NULL,
  `sop_class_uid` varchar(150) DEFAULT NULL,
  `instance_image_type` varchar(100) DEFAULT NULL,
  `series_body_site` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
