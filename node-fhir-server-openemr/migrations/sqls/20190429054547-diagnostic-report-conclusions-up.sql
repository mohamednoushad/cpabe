ALTER TABLE `openemr`.`procedure_report`
ADD COLUMN `conclusion` VARCHAR(250) NULL AFTER `report_notes`,


CREATE TABLE `openemr`.`procedure_conclusion_codes` (
  `codeId` INT NOT NULL AUTO_INCREMENT,
  `conclusion_code` VARCHAR(45) NULL,
  `conclusion_code_term` VARCHAR(250) NULL,
  `procedure_report_id` INT NULL,
  PRIMARY KEY (`codeId`));
