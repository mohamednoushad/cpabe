ALTER TABLE `openemr`.`procedure_order_code`
ADD COLUMN `category_code` VARCHAR(45) NULL AFTER `reason`,
ADD COLUMN `category_name` VARCHAR(100) NULL AFTER `category_code`;

ALTER TABLE `openemr`.`procedure_order_code`
ADD COLUMN `body_site_code` VARCHAR(45) NULL AFTER `category_name`,
ADD COLUMN `body_site_name` VARCHAR(100) NULL AFTER `body_site_code`;

