INSERT INTO `openemr`.`gacl_aro_groups` (`id`, `parent_id`, `lft`, `rgt`, `name`, `value`) VALUES ('19', '10', '14', '15', 'Pharmacist', 'pharmacist');

INSERT INTO `openemr`.`users` (`id`, `username`, `password`, `authorized`, `fname`, `lname`, `facility`, `facility_id`, `see_auth`, `active`, `email_direct`, `cal_ui`, `taxonomy`, `calendar`, `abook_type`, `default_warehouse`, `irnpool`, `main_menu_role`, `patient_menu_role`) VALUES ('10', 'pharmacist', 'NoLongerUsed', '1', 'Ken', 'Follet', 'Abc Clinic', '3', '1', '1', '', '3', '207Q00000X', '1', '', '', '', 'pharmacist', 'standard');

INSERT INTO `openemr`.`users_secure` (`id`, `username`, `password`, `salt`, `last_update`) VALUES ('10', 'pharmacist', '$2a$05$DGmvibdJletCJf5Fe8CzY.QSV.tNdUWIkrvxttZFFuvUGyi8vtbwS', '$2a$05$DGmvibdJletCJf5Fe8CzY/$', '2018-12-10 18:30:45');

INSERT INTO `openemr`.`gacl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES ('16', 'users', 'pharmacist', '10', 'Pharmacist', '0');

INSERT INTO `openemr`.`gacl_groups_aro_map` (`group_id`, `aro_id`) VALUES ('19', '16');

INSERT INTO `openemr`.`groups` (`id`, `name`, `user`) VALUES ('10', 'Default', 'pharmacist');

-- add privileges
INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('28', 'system', '1', '1', 'view', 'Things that pharmacist can only read', '1544446848');
INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('29', 'system', '1', '1', 'addonly', 'Things that pharmacist can read and enter but not modify', '1544446848');
INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('30', 'system', '1', '1', 'wsome', 'Things that pharmacist can read and partly modify', '1544446848');
INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('31', 'system', '1', '1', 'write', 'Things that pharmacist can read and modify', '1544446848');

-- map aro group to acl

INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('28', '19');
INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('29', '19');
INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('30', '19');
INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('31', '19');


-- map privileges to access control objects


INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'patients', 'appt');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'patients', 'notes');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'patients', 'demo');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'encounters', 'coding');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'acct', 'bill');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'acct', 'eob');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'admin', 'drugs');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'patients', 'rx');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'patients', 'med');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'encounters', 'coding_a');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'acct', 'rep_a');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'patients', 'docs');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('31', 'admin', 'practice');


